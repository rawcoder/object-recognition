function Xfeatures = extractFeatures(X, centroids, stride, patchSize, patchMean, covMat)

assert(nargin == 4 || nargin == 6);

numCentroids = size(centroids,1);
imageSize = round(sqrt(size(X,2)));
dataSize = size(X,1);
patchDim = (floor((imageSize-patchSize)./stride) + 1);

Xfeatures = zeros(dataSize, numCentroids*4, 'single');
centroidsSumSq = sumsq(centroids, 2)';

for i=1:dataSize
    if (mod(i,1000) == 0), fprintf(stderr, 'Extracting features: %d / %d\n', i, dataSize); end

	if stride == 1
    	patches = im2col(reshape(X(i,:),[imageSize imageSize]), [patchSize patchSize])';
	else 
		image = reshape(X(i, :), [ imageSize imageSize ]);
		cnt = 1;
		patches = zeros(patchDim.^2, patchSize.^2, 'single');
		for j=1:stride:imageSize-patchSize+1
			for k=1:stride:imageSize-patchSize+1
				patches(cnt, :) = image(j:j+patchSize-1, k:k+patchSize-1)(:)';
				cnt = cnt+1;
			end;
		end;
	end;
	
	% normalization + whitening
	patches = bsxfun(@minus, normalize(patches, 2), patchMean) * covMat;

	distVector = sqrt(bsxfun(@plus, centroidsSumSq, bsxfun(@minus, sumsq(patches,2), 2*patches*centroids')));

	% Use the triangle-activation function to set those distances of the distance vector to zero whose value is greater than mean of the same vector.
	patches = max(bsxfun(@minus, abs(mean(distVector,2)), abs(distVector)), 0);

	patches = reshape(patches, [ patchDim patchDim numCentroids]);

	half = round(patchDim/2);
	% Pool the distance vectors belonging to same quadrant 
	q1 = sum(sum(patches(1:half, 1:half,:),1),2)(:)';
	q2 = sum(sum(patches(1:half, half+1:end,:),1),2)(:)';
	q3 = sum(sum(patches(half+1:end, 1:half,:),1),2)(:)';
	q4 = sum(sum(patches(half+1:end, half+1:end,:),1),2)(:)';

	% Concatenate the resulting vectors into a single feature vector for the image
	Xfeatures(i,:) = [q1 q2 q3 q4];
end;

end;
