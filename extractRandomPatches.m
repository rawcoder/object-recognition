function patches = extractRandomPatches(X, patchSize, numPatches)
% EXTRACTRANDOMPATCHES Extract random patches from X
%	patches = EXTRACTRANDOMPATCHES(X, patchSize, numPatches) extracts 'numPatches'
%	random patches of size (patchSize x patchSize) from X.

patches = zeros(numPatches, patchSize*patchSize, 'single');
[ dataSize dim2 ] = size(X);
imageSize = round(sqrt(dim2));

if mod(numPatches, dataSize) != 0
	fprintf (stderr, '\nWARNING: numPatches not a multiple of dataSize; extraction will not be uniform!!');
end;

patchImage = numPatches/dataSize;
for i=1:dataSize
	r = randi(imageSize - patchSize + 1, patchImage, 1);
	c = randi(imageSize - patchSize + 1, patchImage, 1);
	patch = reshape(X(i, :), [ imageSize imageSize ]);
	for j=1:patchImage
		patches(i*patchImage+j-1,:) = patch(r(j):r(j)+patchSize-1, c(j):c(j)+patchSize-1)(:)';
	end;
end;
