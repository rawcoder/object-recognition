function [X, y, msg] = loadTestData(dataSize)
% LOADTESTDATA Load the CIFAR-10 test data from the .mat file
% 	[X, y, msg]  = LOADTESTDATA(dataSize) loads the test data
%	from .mat file into "X" and "y", where "X" is the data matrix of size:
%	dataSize(defaults to 10000) x 3072 (32x32x3) and "y" is the label
% 	matrix of size: dataSize(defaults to 10000) x 1.
%	If loading fails for some reason, both "X" and "y" are set to empty
%	matrix and "msg" is set with appropriate error message.

% Define the path to CIFAR data folder
CIFAR_DIR = [ pwd() '/cifar-10-batches-mat/' ];

% set some defaults
if ~exist('dataSize', 'var') || isempty(dataSize)
	dataSize = 10000;
end;

X = [];
y = [];
msg = '';

if dataSize < 1 || dataSize > 10000
	msg = 'Error: Invalid argument: dataSize should be in range [1,10000].';
	return
end;

fname = [ CIFAR_DIR 'test_batch' '.mat' ];
if ~exist(fname)
	X = [];
	y = [];
	msg = [ 'Error: File "' fname '" does not exist.' ];
	return;
end;
F = load(fname);
X = [ X; F.data ];
y = [ y; F.labels ];

if size(X, 1) != dataSize
	% Resize to required size
	X = X(1:dataSize, :);
	y = y(1:dataSize, :);
end;

y = y + 1;	% Add 1 to make the range [1,10] (easier to handle during prediction)

end;
